package co.id.danafix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DanafixContractApplication {

	public static void main(String[] args) {
		SpringApplication.run(DanafixContractApplication.class, args);
	}

}
