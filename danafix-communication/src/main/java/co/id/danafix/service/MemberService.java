package co.id.danafix.service;

import co.id.danafix.model.Member;

import java.util.List;

public interface MemberService {
    List<Member> getAllMember();
}
