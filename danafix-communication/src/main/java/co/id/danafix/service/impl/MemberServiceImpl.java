package co.id.danafix.service.impl;

import co.id.danafix.model.Member;
import co.id.danafix.repository.MemberRepository;
import co.id.danafix.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MemberServiceImpl implements MemberService {

    @Autowired
    MemberRepository memberRepository;

    @Override
    public List<Member> getAllMember() {
        return memberRepository.findAll();
    }
}
