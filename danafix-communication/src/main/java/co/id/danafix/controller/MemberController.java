package co.id.danafix.controller;

import co.id.danafix.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/member")
public class MemberController {

    @Autowired
    MemberService memberService;

    @GetMapping(value = "/getMemberAll")
    public ResponseEntity<?> getMemberAll(){
        return ResponseEntity.ok(memberService.getAllMember());
    }

}
