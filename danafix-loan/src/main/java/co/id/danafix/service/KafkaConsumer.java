package co.id.danafix.service;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaConsumer {

    @KafkaListener(topics = "kafka_publish", groupId = "group_id")
    public void consume(String message) {
        System.out.println("Consumed message from service product : " + message);
    }

}
