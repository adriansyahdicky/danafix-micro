package co.id.danafix.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping(value = "publish")
public class PublishSendKafkaController {

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    private static final String TOPIC = "kafka_publish";

    @GetMapping("/posting/{name}")
    public String post(@PathVariable("name") final String name) {
        String jsonObject = "{\"uuid\" : \""+UUID.randomUUID().toString()+"\", \""+name+"\": \"name\", \"address\": \"Jakarta\"}";
        kafkaTemplate.send(TOPIC, jsonObject);
        return "Published successfully";
    }

}
