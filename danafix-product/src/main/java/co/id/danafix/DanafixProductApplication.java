package co.id.danafix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication(scanBasePackages = {"co.id.danafix"})
@Configuration
@EnableAutoConfiguration
public class DanafixProductApplication {

	public static void main(String[] args) {
		SpringApplication.run(DanafixProductApplication.class, args);
	}

}
