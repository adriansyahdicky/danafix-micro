package co.id.danafix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DanafixGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(DanafixGatewayApplication.class, args);
	}

}
