package co.id.danafix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DanafixTelephonyApplication {

	public static void main(String[] args) {
		SpringApplication.run(DanafixTelephonyApplication.class, args);
	}

}
