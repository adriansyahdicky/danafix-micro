package co.id.danafix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DanafixLenderApplication {

	public static void main(String[] args) {
		SpringApplication.run(DanafixLenderApplication.class, args);
	}

}
